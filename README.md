<h1 align="center">Welcome to trombinoscope-symfony-6 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0--alpha-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/php-%3E%3D8.1-blue.svg" />
  <img src="https://img.shields.io/badge/symfony-%3E%3D6.1-blue.svg" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Description soon...

## Prerequisites

- php >=8.1
- symfony >=6.1

## Install

```sh
npm install
```

## Author

👤 **Nathan GUIRADO-PATRICO**


## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_