<?php

namespace App\DataFixtures;

use App\Entity\Users;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class UsersFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        // create 20 users! Bam!
        for ($i = 0; $i < 10; $i++) {
            $user = new Users();
            $user->setNom($faker->name());
            $user->setPrenom($faker->name());
            $user->setThumbnail($faker->imageUrl(360, 360, 'animals', true, 'dogs', true, 'jpg'));
            $user->setMail($faker->email());
            $user->setMobile($faker->phoneNumber());
            $user->setFix(null);
            $user->setFonction($faker->jobTitle());
            $user->setDetails($faker->sentence());
            $user->setService($faker->word());
            $user->setBatiment($faker->word());
            $user->setSite($faker->word());

            $manager->persist($user);
        }

        $manager->flush();
    }
}
